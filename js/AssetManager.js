class AssetManager {

  constructor() {
    this.assets = [];
    this.queue = [];
  }

  addItem(item = null) {
    if (item) {
      if (item.src && item.id) {
        // Check if item is present and has sufficient data.
        const allowedTypes = ['image'];
        if (allowedTypes.indexOf(item.type) !== -1) {
          let buffer;
          // Create objects according to Item's type
          switch (item.type) {
            case 'image':
              buffer = new Image();
              buffer.src = item.src;
              break;
            default:
              return new Error('Unsupported asset type');
          }
          buffer.loaded = false;
          buffer.id = item.id;
          buffer.onload = () => buffer.loaded = true;
          this.queue.push(buffer);
        }
      }
    }
  }

  loadItems() {
    return new Promise(resolve => {
      const interval = setInterval(() => {
        let loaded = true;
        let ii = 0;
        let loadedIndices = [];
        // Loop through queue and check if item is loaded.
        this.queue.forEach((item) => {
          if (item.loaded) {
            // Add loaded items to assets and
            // note them for deletion from queue.
            this.assets[item.id] = item;
            loadedIndices.push(ii);
          }
          // If any item is not loaded yet, set the switch.
          else {
            loaded = false;
          }
          ii++;
        });
        // Delete loaded Items from queue, beginning from end.
        for (let ii = loadedIndices.length -1; ii >= 0; ii--) {
          this.queue.splice(loadedIndices[ii], 1);
        }
        // Resolve all assets when loading is fully done.
        if (loaded) {
          clearInterval(interval);
          resolve(this.assets);
        }
      }, 10);
    });
  }

  async serve() {
    // Start loading Items
    const assets = await assetMgr.loadItems();
    // serve assets
    return new Promise((resolve) => resolve(assets));
  }
}