'use strict';

// Create instance of AssetManager
let assetMgr = new AssetManager();

// Add items to load
let item = {
  src : "https://upload.wikimedia.org/wikipedia/commons/9/95/Big_Pine_landscape.jpg",
  id : 'test',
  type : 'image'
};
let item2 = {
  src : "https://upload.wikimedia.org/wikipedia/commons/9/95/Big_Pine_landscape.jpg",
  id : 'test2',
  type : 'image'
};
assetMgr.addItem(item);
assetMgr.addItem(item2);

// Immediately-invoked function, drawing images on canvas as soon as loading is completed.
(async () => {
  let assets = await assetMgr.serve();
  let canvas = document.getElementById('canvas');
  let context = canvas.getContext('2d');
  context.drawImage(assets['test'], 0, 0);
  context.drawImage(assets['test2'], 100, 100);
})();